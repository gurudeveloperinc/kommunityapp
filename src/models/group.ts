import { StaffModel } from './staffmodel';
export class Group{
    public gid: string;
    public gname: string;
    public gmang: string;
    public group_mgr: StaffModel

    constructor(gid?: string,gname?: string,gmang?: string) {
    }

    JSONGroup(json: string){
        var arr = JSON.parse(json);
        this.gid = arr['group_id'];
        this.gname = arr['group_name'];
        this.gmang = arr['group_manager'];
        if(this.gmang != "No one"){
            this.group_mgr = new StaffModel().JSONStaff(JSON.stringify(arr['group_mgr_det']));
        }
        return this;
    }
}