export class StaffModel{
    public uid: string;
    public uname: string;
    public fname: string;
    public lname: string;
    public fullname: string;
    public email: string;
    public usertype: number;
    public pass: string;
    public joined: string;
    public pphoto: string;

    constructor() {
    }

     JSONStaff(json: string){
        var arr = JSON.parse(json);
        this.uid = arr['staff_id'];
        this.uname = arr['user_name'];
        this.fname = arr['first_name'];
        this.lname = arr['last_name'];
        this.fullname = arr['full_name'];
        this.email = arr['email'];
        this.usertype = arr['user_type'];
        this.pass = arr['pass'];
        this.joined = arr['joined_on'];
        this.pphoto = arr['pphoto'];
        return this;
    }
}