export class Message{
    
    // constructor(public id: number, public from: number, public to: number, public msgtime: string
    //             ,public msgtext: string, public readstatus: boolean){
    //
    // }
    constructor(public mid: number,
                public sender: number,
                public receiver: number,
                public created_at: string,
                public content: string,
                public isRead: any){

    }
}