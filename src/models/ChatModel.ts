import { Message } from './Message';
import {IUser} from "../interfaces/user.interface";
export class ChatModel {
    public chatWith: IUser;
    public lastmsg: Message;
    public messages: Array<Message>;
    public unreadCount: number;
}