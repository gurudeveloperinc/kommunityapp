import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {IUser} from "../../interfaces/user.interface";
import {FileTransfer, FileTransferObject, FileUploadOptions} from "@ionic-native/file-transfer";

/*
  Generated class for the UsersProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UsersProvider {

    url:string = "https://kommunity.gurudeveloperinc.com/";
    // url:string = "http://192.168.8.100:8000/";
    // url:string = "http://localhost:8000/";
    user:IUser;
    kommunity:string = 'kommunity';

    constructor(public http: HttpClient,
                private transfer: FileTransfer,) {
    }


    signIn(user) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(user);
        return this.http.post(this.url + "api/sign-in",body,{headers});
    }

    signUp(user) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(user);
        return this.http.post(this.url + "api/sign-up",body,{headers});
    }


    getKinPosts(){
        let data = {'uid' : this.user.uid};
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "api/kin-posts",body,{headers});
    }

    getKommunityPosts(){
        let data = {};
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "api/kommunity-posts",body,{headers});
    }

    changePassword(user) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(user);
        return this.http.post(this.url + "api/change-password",body,{headers});
    }


    editProfile(user) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(user);
        return this.http.post(this.url + "api/edit-profile",body,{headers});
    }

    profile(user) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(user);
        return this.http.post(this.url + "api/profile",body,{headers});
    }

    events(user) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(user);
        return this.http.post(this.url + "api/events",body,{headers});
    }

    categories() {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify({});
        return this.http.post(this.url + "api/categories",body,{headers});
    }

    categoryPosts(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "api/category-posts",body,{headers});
    }

    updateProfile(){
        this.profile(this.user).subscribe((success:any)=>{
            console.log(success);
            this.user = success.data;
        },(error)=>{console.log(error)});
    }

    post(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "api/post",body,{headers});
    }

    postSearch(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "api/post-search",body,{headers});
    }

    deletePost(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "api/delete-post",body,{headers});
    }

    updateDetails(){
        let data = {'uid' : this.user.uid};
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        this.http.post(this.url + "api/profile",body,{headers}).subscribe((success:any)=>{
            console.log(success);
            this.user = success.data;
        });
    }

    updateDetailsWithPromise(){
        let data = {'uid' : this.user.uid};
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "api/profile",body,{headers});
    }
    follow(data){
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "api/follow-user",body,{headers});

    }

    unfollow(data){
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "api/unfollow-user",body,{headers});

    }

    sendMessage(data){
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "api/send-message",body,{headers});

    }

    conversation(data){
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "api/conversation",body,{headers});

    }

    messageUsers(data){
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "api/message-users",body,{headers});
    }

    setMessageRead(data){
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "api/set-message-read",body,{headers});
    }

    deleteMessage(data){
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "api/delete-message",body,{headers});
    }

    deleteConversation(data){
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "api/delete-conversation",body,{headers});
    }

    upload(images,catid,pid){

        let options: FileUploadOptions = {
            fileKey: 'image',
            fileName: 'upload.jpg',
            params : {
                'pid' : pid,
                'catid' : catid,
            }
        };

        const fileTransfer: FileTransferObject = this.transfer.create();

        for(let i = 0; i < images.length; i++){
            fileTransfer.upload(images[i], this.url + 'api/post-image', options)
                .then((success:any) => {

                    console.log(success);

                }, (error) => {
                    // error
                    console.log(error);
                })

        }

    }



}
