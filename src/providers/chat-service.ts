import { Message } from './../models/Message';
import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import { map } from 'rxjs/operators/map';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import {IUser} from "../interfaces/user.interface";


@Injectable()
export class ChatService {

  constructor(private http: HttpClient,
              private events: Events) {
  }

  mockNewMsg(msg) {
    const mockMsg: Message = new Message(Date.now(), 210000198410281948, 140000198202211138, Date.now().toString(), msg.msgtext, true);
      

    setTimeout(() => {
      this.events.publish('chat:received', mockMsg, Date.now())
    }, Math.random() * 1800)
  }

  getMsgList(): Observable<Message[]> {
    const msgListUrl = './assets/mock/msg-list.json';
    return this.http.get<any>(msgListUrl)
    .pipe(map(response => response.array));
  }

  sendMsg(msg: Message) {
    return new Promise(resolve => setTimeout(() => resolve(msg), Math.random() * 1000))
    .then(() => this.mockNewMsg(msg));
  }

  getUserInfo(): Promise<IUser> {
    const userInfo: IUser = new IUser();
    return new Promise(resolve => resolve(userInfo));
  }

  converMsgArr(arr: any[]): Array<Message>{
    var msgList = Array<Message>();
    arr.forEach((item: any) =>{
        msgList.push(new Message(item.id, item.from, item.to, item.time, item.msgtext, item.readstatus));
    });
    return msgList;
  }

}