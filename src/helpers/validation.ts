export class Validation{

    static isValidEmail(data){
        let  regexp = new RegExp('[A-Za-z0-9]+@[a-z0-9]+[.][a-z]{2,5}');
        let    test = regexp.test(data);

        return !(data == null || data == "" || !test);
    }

    static isEmpty(data){
        return (data == null || data == "");
    }

    static isValidString(data){
        let  regexp = new RegExp('[a-zA-Z]+');
        let    test = regexp.test(data);

        return !(data == null || data == "" || !test);
    }

    static isValidAlphaNumeric(data){
        let  regexp = new RegExp('[a-zA-Z0-9]+');
        let    test = regexp.test(data);

        return !(data == null || data == "" || !test);
    }

    static isValidNumber(data){
        let  regexp = new RegExp('[0-9]+');
        let    test = regexp.test(data);
        return !(data == null || data == "" || !test);
    }


}