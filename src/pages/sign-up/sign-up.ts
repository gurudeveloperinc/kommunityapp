import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {IUser} from "../../interfaces/user.interface";
import {UsersProvider} from "../../providers/users/users";
import {TabsPage} from "../tabs/tabs";
import {Storage} from "@ionic/storage";

@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {

    loading:boolean = false;
    error:string;
    username:string;
    name:string;
    email:string = 'toby.okeke@gmail.com';
    phone:string;
    password:string = '123456';
    passwordError:string;
    confirmPassword:string;
    isSignUp:boolean = false;

    constructor(public navCtrl: NavController,
                public userProvider: UsersProvider,
                public storage: Storage,
                public alertCtrl: AlertController,
                public navParams: NavParams) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SignUpPage');
    }


    signIn(){
        let user = new IUser();
        user.email = this.email;
        user.password = this.password;

        this.loading = true;
        this.userProvider.signIn(user).subscribe((success:any)=>{

            console.log(success);
            if( success == null || success == 0) {
                this.showAlert('Error',"Wrong credentials!");
            } else {

                this.storage.set('isSignedIn',true);
                this.storage.set('user',success);
                this.userProvider.user = success;
                this.navCtrl.setRoot(TabsPage);
            }


        },(error)=>{
            console.log(error);
            this.showAlert('Error',"Something went wrong! Please try again");
        });

    }

    switch(){
        this.isSignUp = !this.isSignUp;
    }

    signUp() {


        let user = new IUser();

        user.username = this.username;
        user.name = this.name;
        user.email = this.email;
        user.password = this.password;
        user.phone = this.phone;

        this.loading = true;
        this.userProvider.signUp(user).subscribe((success:any) => {

            console.log(success);

            if(success.message > 0){
                user.uid = success.message;

                this.showAlert("Successful","Your account was created successfully");
                this.userProvider.user = user;
                this.navCtrl.setRoot(TabsPage);
            } else {
                this.showAlert('Error',"Something went wrong! Please try again");
            }

        },(error)=>{
            console.log(error);
            this.showAlert('Error',"Something went wrong! Please try again");
        });

    }

    setError(error){
        this.error = error;
        this.loading = false;
        let self = this;
        this.showAlert('Error',error);
        setTimeout(function () {
            self.error = null;
        },5000);
    }

    showAlert(title,message) {
        this.loading = false;
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }


}
