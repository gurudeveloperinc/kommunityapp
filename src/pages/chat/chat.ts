import {ChatService} from '../../providers/chat-service';
import {Message} from '../../models/Message';
import {ChatModel} from '../../models/ChatModel';
import {Component, ElementRef, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, Content, Events} from 'ionic-angular';
import {IUser} from "../../interfaces/user.interface";
import {UsersProvider} from "../../providers/users/users";

/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-chat',
    templateUrl: 'chat.html',
})
export class ChatPage {

    @ViewChild(Content) content: Content;
    @ViewChild('chat_input') messageInput: ElementRef;
    @ViewChild('footer') cfooter: ElementRef;
    msgList:any[] = null;

    user = new IUser();
    toUser = new IUser();

    editorMsg = '';
    showEmojiPicker = false;
    chatModel: ChatModel;
    oldInputHeight: number = 0;



    constructor(public navCtrl: NavController,
                public userProvider:UsersProvider,
                public navParams: NavParams, private chatService: ChatService,
                private events: Events) {
        console.log(this.navParams);
        this.chatModel = this.navParams.data;
        this.toUser = this.chatModel.chatWith;
        this.user.uid = this.userProvider.user.uid;

    }

    ionViewDidLeave() {
        this.events.unsubscribe('chat:received');
    }

    ionViewDidLoad() {
        this.getMsg();
        console.log('ionViewDidLoad ChatPage');
        // Subscribe to received  new message events
        this.events.subscribe('chat:received', msg => {
            this.pushNewMsg();
        })
    }

    viewPP() {
        //this.navCtrl.push();
    }

    /**
     * @name getMsg
     * @returns {Promise<ChatMessage[]>}
     */

    getMsg() {
        let data = {
            'sender' : this.userProvider.user.uid,
            'receiver' : this.toUser.uid
        };

        return this.userProvider.conversation(data)
             .subscribe((res:any[]) => {
                this.msgList =  res;
                this.scrollToBottom();
            });
    }

    // getMsg() {
    //     // Get mock message list
    //     return this.chatService
    //         .getMsgList()
    //         .subscribe(res => {
    //             this.msgList = this.chatService.converMsgArr(res);
    //             this.scrollToBottom();
    //         });
    // }
    //
    scrollToBottom() {
        setTimeout(() => {
            if (this.content.scrollToBottom) {
                this.content.scrollToBottom();
            }
        }, 400)
    }

    scrollFastToBottom() {
        setTimeout(() => {
            if (this.content.scrollToBottom) {
                this.content.scrollToBottom();
            }
        }, 100)
    }

    private focus() {
        if (this.messageInput && this.messageInput.nativeElement) {
            this.messageInput.nativeElement.focus();
        }
    }

    private setTextareaScroll() {
        const textarea = this.messageInput.nativeElement;
        textarea.scrollTop = textarea.scrollHeight;
    }

    /**
     * @name sendMsg
     */
    sendMsg() {
        if (!this.editorMsg.trim()) return;

        this.pushNewMsg();

        this.editorMsg = '';

        if (!this.showEmojiPicker) {
            this.focus();
        }

    }

    getMsgIndexById(id: number) {
        return this.msgList.findIndex(e => e.id === id)
    }

    onFocus() {
        this.content.resize();
        this.scrollToBottom();
    }

    /**
     * @name pushNewMsg
     * @param msg
     */
    pushNewMsg() {

        let data = {
            'sender' : this.userProvider.user.uid,
            'receiver': this.toUser.uid,
            'content' : this.editorMsg
        };

        this.userProvider.sendMessage(data).subscribe((success)=>{
            this.scrollToBottom();
            this.getMsg();
        },(error)=>{console.log(error)});

    }
    //
    // pushNewMsg(msg: Message) {
    //     const userId = this.user.uid,
    //         toUserId = this.toUser.uid;
    //     // Verify user relationships
    //     if (msg.from === userId && msg.to === toUserId) {
    //         this.msgList.push(msg);
    //     } else if (msg.to === userId && msg.from === toUserId) {
    //         this.msgList.push(msg);
    //     }
    //     this.scrollToBottom();
    // }
    //
    pop() {
        this.navCtrl.pop();
    }


}
