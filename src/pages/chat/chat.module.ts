import { InputDivComponentModule } from '../../components/input-div/input-div.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatPage } from './chat';

@NgModule({
  declarations: [
    ChatPage,
  ],
  imports: [
    IonicPageModule.forChild(ChatPage),
    InputDivComponentModule
  ],
  exports: [
    ChatPage
  ]
})
export class ChatPageModule {}
