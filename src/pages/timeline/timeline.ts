import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {MemberprofilePage} from "../memberprofile/memberprofile";

/**
 * Generated class for the TimelinePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-timeline',
  templateUrl: 'timeline.html',
})
export class TimelinePage {

    items:any[];
    type:string;

    constructor(public navCtrl: NavController, public navParams: NavParams) {

        this.type = this.navParams.get('type');
        this.items = this.navParams.get('items');
    }

    ionViewDidLoad() {
        this.items.reverse();
    console.log('ionViewDidLoad TimelinePage');
    }

    pop(){
        this.navCtrl.pop();
    }

    viewProfile(profile){
        this.navCtrl.push(MemberprofilePage,{'profile' : profile});
    }

}
