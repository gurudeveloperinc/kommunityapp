import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {UsersProvider} from "../../providers/users/users";

/**
 * Generated class for the MemberprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-memberprofile',
  templateUrl: 'memberprofile.html',
})
export class MemberprofilePage {

    basicProfile:any = null;
    profile:any;
    loading:boolean = false;
    isFollowing:boolean = false;
    constructor(public navCtrl: NavController,
                public alertCtrl: AlertController,
                public userProvider: UsersProvider,
                public navParams: NavParams) {
        this.basicProfile = this.navParams.get('profile');
    }

    ionViewDidEnter() {
        console.log('ionViewDidLoad MemberprofilePage');

        this.getFullProfile();
        this.checkFollowStatus();
    }

    checkFollowStatus(){
        let following:any[] = this.userProvider.user.following;
        this.isFollowing = false;
        following.forEach(value => {
            if(value.uid == this.basicProfile.uid) {
                this.isFollowing = true;
                return;
            }

        })
    }

    getFullProfile(){
        let user = {'uid' : this.basicProfile.uid};
        this.loading = true;
        this.userProvider.profile(user).subscribe((success:any)=>{
            this.loading = false;
            this.profile = success.data;
        },(error)=>{
            this.loading = false;
            console.log(error);
        });
    }

    pop(){
        this.navCtrl.pop();
    }

    follow(){
        let data = {
            'uid' : this.userProvider.user.uid,
            'following' : this.profile.uid
        };

        this.loading = true;

        this.userProvider.follow(data).subscribe((success)=>{

            this.loading = false;
            this.getFullProfile();
            this.userProvider.updateDetailsWithPromise().subscribe((success:any)=>{

                this.userProvider.user = success.data;
                this.checkFollowStatus();
            });

        },(error)=>{
            this.showAlert("Error","Please try again");
            this.loading = false;
            console.log(error)}
        );
    }

    unfollow(){
        let data = {
            'uid' : this.userProvider.user.uid,
            'following' : this.profile.uid
        };

        this.loading = true;
        this.userProvider.unfollow(data).subscribe((success)=>{

            this.loading = false;
            this.getFullProfile();
            this.userProvider.updateDetailsWithPromise().subscribe((success:any)=>{
                this.userProvider.user = success.data;
                this.checkFollowStatus();
            });

            //     this.userProvider.updateDetails();
            // this.checkFollowStatus();
        },(error)=>{
            this.showAlert("Error","Please try again");
            this.loading = false;
            console.log(error)}
        );
    }


    showAlert(title,message) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }


}
