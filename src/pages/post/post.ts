import { Component } from '@angular/core';
import {ActionSheetController, AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Camera, CameraOptions} from "@ionic-native/camera";
import {ImagePicker} from "@ionic-native/image-picker";
import {UsersProvider} from "../../providers/users/users";
import {FileTransfer, FileTransferObject, FileUploadOptions} from "@ionic-native/file-transfer";

/**
 * Generated class for the PostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-post',
  templateUrl: 'post.html',
})
export class PostPage {

    images: string[] = [];
    catid : number;
    content: string;
    categories : string[];
    pid : number;
    loading : boolean = false;
    isKin: any = false;
    showimage:any;

    constructor(public navCtrl: NavController,
                private camera: Camera,
                public alertCtrl: AlertController,
                private imagePicker: ImagePicker,
                public actionSheetCtrl: ActionSheetController,
                private userProvider : UsersProvider,
                private transfer: FileTransfer,
                public navParams: NavParams) {
    }

    ionViewWillEnter(){
        if(this.loading == false){
            this.images = [];
            this.content = "";
        }

        this.getCategories();
        this.photoAction();
    }


    ionViewDidLoad() {
        console.log('ionViewDidLoad PostPage');
    }

    getCategories(){

        this.loading = true;
        this.userProvider.categories().subscribe((success:any[])=>{
            this.loading = false;
            console.log(success);

            this.categories = success.reverse();
        },(error)=>{console.log(error)});
    }


    takePhoto() {

        const options: CameraOptions = {
            quality: 100,
            saveToPhotoAlbum: false,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            targetWidth: 800,
            targetHeight: 800
        };

        this.camera.getPicture(options).then((imagePath) => {

            // let currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
            // let correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);

            console.log(imagePath);
            this.images.push(imagePath);


        }, (error) => {
            console.log(error)
            // Handle error
        });
    }

    upload(){

        this.loading = true;

        let options: FileUploadOptions = {
            fileKey: 'image',
            fileName: 'upload.jpg',
            params : {
                'uid' : this.userProvider.user.uid,
                'catid' : this.catid,
                'content' : this.content,
                'isKin' : this.isKin,
            }
        };

        let self = this;

        const fileTransfer: FileTransferObject = this.transfer.create();

        fileTransfer.upload(this.images[0], this.userProvider.url + 'api/post', options)
            .then((success:any) => {

                self.loading = false;
                self.navCtrl.pop();
                self.showAlert("Success","Post created.");
                self.loading = false;
                console.log(success);

            }, (error) => {

                self.showAlert("Error","Please try again.");

                // error
                console.log(error);
                self.loading = false;

            })


    }


    uploadPhoto(){

        let options = {
            maximumImagesCount: 1,
            width: 800,
            height: 800,
            quality: 100
        };

        this.imagePicker.getPictures(options).then((results) => {

            for (let i = 0; i < results.length; i++) {

                this.images.push(results[i]);
            }

        }, (err) => {
            this.showAlert("Error","Failed to select photo. Please try again.");
        });
    }

    photoAction() {

        let actionSheet = this.actionSheetCtrl.create({
            title: 'Please confirm your action',
            buttons: [
                {
                    text: 'Take Photo',
                    handler: () => {
                        this.takePhoto();
                    }
                },{

                    text: 'Upload Photo',
                    handler: () => {
                        this.uploadPhoto();
                    }
                },{
                    text: 'Close Menu',
                    role: 'cancel',
                    handler: () => {
                        //
                    }
                }
            ]
        });
        actionSheet.present();
    }

    setKinPost(){
        this.isKin = 1;
    }

    setKommunityPost(){
        this.isKin = 0;
    }

    showAlert(title,message) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }




}
