import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {UsersProvider} from "../../providers/users/users";

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {


    searchQuery: string = '';
    items: string[];
    loading:boolean = false;

    constructor(public navCtrl: NavController,
                public userProvider: UsersProvider,
                public navParams: NavParams) {
    }

    ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
    }



    search(ev:any) {
        this.searchQuery = ev.target.value;

        let data = {
            'term' : this.searchQuery
        };

        this.loading = true;
        this.userProvider.postSearch(data).subscribe((success:any[])=>{
            this.loading = false;
            console.log(success);
            this.items = success;
        },(error)=>{
            this.loading = false;
            console.log(error);
        });

    }

    getItems(ev: any) {

        // set val to the value of the searchbar
        let val = ev.target.value;

        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.items.filter((item) => {
                return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })
        }
    }

}
