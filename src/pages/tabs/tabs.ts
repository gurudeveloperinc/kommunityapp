import { Component } from '@angular/core';
import { HomePage } from '../home/home';
import {SearchPage} from "../search/search";
import {PostPage} from "../post/post";
import {EventsPage} from "../events/events";
import {NavController} from "ionic-angular";
import {ProfilePage} from "../profile/profile";
import {NotificationsPage} from "../notifications/notifications";
import {isUndefined} from "ionic-angular/util/util";
import {UsersProvider} from "../../providers/users/users";
import {SignUpPage} from "../sign-up/sign-up";
import {ChatlistPage} from "../chatlist/chatlist";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

    selected:number = 0;
    tab1Root = HomePage;
    tab2Root = SearchPage;
    tab3Root = ChatlistPage;
    tab4Root = EventsPage;
    tabsPlacement:string = "bottom";

    constructor(public navCtrl: NavController,
                public userProvider: UsersProvider,
                ) {
       if(isUndefined(this.userProvider.user) || this.userProvider.user == null ) this.navCtrl.setRoot(SignUpPage);
    }

    goToProfile(){
        this.navCtrl.push(ProfilePage);
    }

    goToPosts(){
        this.navCtrl.push(PostPage);
    }

    goToNotifications(){
        this.navCtrl.push(NotificationsPage);
    }

    getKommunityPosts(){
        console.log(this.userProvider.kommunity);
    }
    getKinPosts(){

        console.log(this.userProvider.kommunity);
    }

    onTabSelect(event:any){
        this.selected = event.index;
    }
}
