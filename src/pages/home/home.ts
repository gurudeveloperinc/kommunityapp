import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {UsersProvider} from "../../providers/users/users";
import {isUndefined} from "ionic-angular/util/util";
import {MemberprofilePage} from "../memberprofile/memberprofile";
import {CategoriesPage} from "../categories/categories";
import {TimelinePage} from "../timeline/timeline";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

    loading:boolean = true;
    kommunityPosts:any[];
    kinPosts:any[];
    refresher:any;
    categories:string[];

    constructor(public navCtrl: NavController,
                public userProvider: UsersProvider) {

    }

    ionViewDidLoad(){
        this.getCategories();
        this.getKommunityPosts();
        this.getKinPosts();
    }

    swipeRight() {
        this.navCtrl.parent.select(2);
    }

    swipeLeft() {
        this.navCtrl.parent.select(1);
    }

    refresh(refresher){
        this.refresher = refresher;

        if(this.userProvider.kommunity == 'kommunity') this.getKommunityPosts();
        else this.getKinPosts();
    }


    viewProfile(profile){
        console.log(profile);
        this.navCtrl.push(MemberprofilePage,{'profile' : profile});
    }


    getKommunityPosts(){

        if(!isUndefined(this.refresher) ) this.refresher.complete();


        this.loading = true;
        this.userProvider.getKommunityPosts().subscribe((success:any)=>{
            this.loading = false;

            this.kommunityPosts = success.data.reverse();
            console.log(success);

        },(error)=>{
            this.loading = false;
            console.log(error);
        });
    }

    getKinPosts(){
        this.loading = true;
        this.userProvider.getKinPosts().subscribe((success:any)=>{
            this.loading = false;

            this.kinPosts = success.data.reverse();
            console.log(success);

        },(error)=>{
            this.loading = false;
            console.log(error);
        });
    }

    goToTimeline(){
        this.navCtrl.push(TimelinePage,{'type' : 'post', 'items' : this.kommunityPosts });
    }

    getCategories(){

        this.loading = true;
        this.userProvider.categories().subscribe((success:any[])=>{
            this.loading = false;
            console.log(success);

            this.categories = success.reverse();
        },(error)=>{console.log(error)});
    }


    goToCategories(){
        this.navCtrl.setRoot(CategoriesPage);
    }

    viewSingleCategory(category){

        let data = {
            'catid' : category.catid
        };

        this.userProvider.categoryPosts(data).subscribe((success:any)=>{

            console.log(success);
            this.navCtrl.push(TimelinePage,{'type' : 'post', 'items' : success.posts });
        },(error)=>{console.log(error)});

    }


}
