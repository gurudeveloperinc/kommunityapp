import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {UsersProvider} from "../../providers/users/users";
import {TimelinePage} from "../timeline/timeline";

/**
 * Generated class for the EventsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-events',
  templateUrl: 'events.html',
})
export class EventsPage {

    events:any[];
    loading:boolean = false;

    constructor(public navCtrl: NavController,
              public userProvider: UsersProvider,
              public navParams: NavParams) {
    }


    ionViewDidEnter() {
        console.log('ionViewDidLoad EventsPage');
        this.getEvents();
    }

    getEvents(){
        let data = {};
        this.loading = true;
        this.userProvider.events(data).subscribe((success:any)=>{
            this.loading = false;
            let events:any[] = success.data;

            this.events = events.reverse();
        },(error)=>{console.log(error)});
    }

    eventDetails(event){
        this.navCtrl.push(TimelinePage,{'type': 'event','items' : event});
        console.log(event);
    }


}
