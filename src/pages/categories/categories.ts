import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {UsersProvider} from "../../providers/users/users";

/**
 * Generated class for the CategoriesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html',
})
export class CategoriesPage {

    loading:boolean = false;
    categories:any[];
    constructor(public navCtrl: NavController,
                public userProvider: UsersProvider,
                public navParams: NavParams) {
    }

    ionViewDidLoad() {
        this.getCategories();
    console.log('ionViewDidLoad CategoriesPage');
    }

    getCategories(){

        this.loading = true;
        this.userProvider.categories().subscribe((success:any[])=>{
            this.loading = false;
            console.log(success);

            this.categories = success.reverse();
        },(error)=>{console.log(error)});
    }

}
