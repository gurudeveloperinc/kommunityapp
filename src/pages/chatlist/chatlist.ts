import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ChatModel } from '../../models/ChatModel';
import { Message } from '../../models/Message';
import {IUser} from "../../interfaces/user.interface";
import {UsersProvider} from "../../providers/users/users";

/**
 * Generated class for the ChatlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chatlist',
  templateUrl: 'chatlist.html',
})
export class ChatlistPage {

  chats = Array<ChatModel>();
  loading:boolean = false;

  constructor(public navCtrl: NavController,
              public userProvider:UsersProvider,
              public navParams: NavParams) {

  }

  ionViewDidEnter(){
      this.updateChats();
  }

  updateChats(){
      let data = {
          'uid' : this.userProvider.user.uid
      };
      this.loading = true;
      this.userProvider.messageUsers(data).subscribe((success:any[])=>{
          this.chats = Array<ChatModel>();
          for(let i =0; i < success.length; i++){
              let c = new ChatModel();
              c.chatWith = new IUser();
              c.chatWith.uid = success[i].uid;
              c.chatWith.username =  success[i].username;
              c.chatWith.email =  success[i].email;
              c.chatWith.phone = success[i].phone;
              c.chatWith.image = success[i].image;
              c.lastmsg = success[i].lastMessage;
              c.unreadCount = success[i].unread;
              this.chats.push(c);
              this.loading = false;

          }

      },(error)=>{
          console.log(error);
          this.loading = false;
      });
  }

  ionViewDidLoad() {
  }

  noChats(){
    return this.chats.length <= 0;
  }

}
