export class IUser{

    public uid?:number;
    public image:string;
    public username:string;
    public name:string;
    public email:string;
    public password?:string;
    public phone:string;
    public bio?:string;
    public posts:any[];
    public followers:string[];
    public following:string[];
    public kins:string;


}