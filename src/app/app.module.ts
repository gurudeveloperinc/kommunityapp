import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {IonicApp, IonicModule, IonicErrorHandler, AlertController} from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { UsersProvider } from '../providers/users/users';
import { Camera} from "@ionic-native/camera";
import { SuperTabsModule} from "ionic2-super-tabs";
import {EventsPageModule} from "../pages/events/events.module";
import {PostPageModule} from "../pages/post/post.module";
import {SearchPageModule} from "../pages/search/search.module";
import {ImagePicker} from "@ionic-native/image-picker";
import {ProfilePageModule} from "../pages/profile/profile.module";
import {NotificationsPageModule} from "../pages/notifications/notifications.module";
import {HttpClientModule} from "@angular/common/http";
import {SignUpPageModule} from "../pages/sign-up/sign-up.module";
import {IonicStorageModule} from "@ionic/storage";
import {FileTransfer} from "@ionic-native/file-transfer";
import {TimelinePageModule} from "../pages/timeline/timeline.module";
import {MemberprofilePageModule} from "../pages/memberprofile/memberprofile.module";
import {CategoriesPageModule} from "../pages/categories/categories.module";
import {ChatService} from "../providers/chat-service";
import {ChatPageModule} from "../pages/chat/chat.module";
import {ChatlistPageModule} from "../pages/chatlist/chatlist.module";


@NgModule({
    declarations: [
        MyApp,
        AboutPage,
        ContactPage,
        HomePage,
        TabsPage
    ],
    imports: [
        HttpClientModule,
        EventsPageModule,
        PostPageModule,
        SignUpPageModule,
        SearchPageModule,
        BrowserModule,
        NotificationsPageModule,
        ProfilePageModule,
        TimelinePageModule,
        CategoriesPageModule,
        ChatPageModule,
        ChatlistPageModule,
        MemberprofilePageModule,
        IonicStorageModule.forRoot(),
        IonicModule.forRoot(MyApp),
        SuperTabsModule.forRoot(),
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        AboutPage,
        ContactPage,
        HomePage,
        TabsPage,
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        UsersProvider,
        Camera,
        ImagePicker,
        FileTransfer,
        AlertController,
        ChatService

    ]
})
export class AppModule {}
