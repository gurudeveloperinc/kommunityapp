import { InputDivComponent } from './input-div';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
  declarations: [
    InputDivComponent,
  ],
  imports: [
    IonicPageModule.forChild(InputDivComponent),
  ],
  exports: [
    InputDivComponent
  ]
})
export class InputDivComponentModule {
}