import { Component } from '@angular/core';

/**
 * Generated class for the InputDivComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'input-div',
  templateUrl: 'input-div.html'
})
export class InputDivComponent {

  text: string;

  constructor() {
    console.log('Hello InputDivComponent Component');
    this.text = 'Hello World';
  }

}
