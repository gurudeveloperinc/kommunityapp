import { NgModule } from '@angular/core';
import { InputDivComponent } from './input-div/input-div';
import { ApploaderComponent } from './apploader/apploader';
@NgModule({
	declarations: [InputDivComponent,
    ApploaderComponent],
	imports: [],
	exports: [InputDivComponent,
    ApploaderComponent]
})
export class ComponentsModule {}
