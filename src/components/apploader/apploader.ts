import { Component, Input } from '@angular/core';

/**
 * Generated class for the ApploaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */


@Component({
  selector: 'apploader',
  templateUrl: 'apploader.html'
})
export class ApploaderComponent {

  @Input() text: string;

  constructor() {
    this.text = 'Loading...';
  }

}
