import { IonicPageModule } from 'ionic-angular';
import { NgModule } from '@angular/core';
import { ApploaderComponent } from './apploader';

@NgModule({
    declarations:[
        ApploaderComponent
    ],
    imports: [
        IonicPageModule.forChild(ApploaderComponent)
    ],
    exports: [
        ApploaderComponent
    ]
})


export class ApploaderComponentModule{}